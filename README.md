
<!-- README.md is generated from README.Rmd. Please edit that file -->

# rdocs

The goal of rdocs is to provide templates for the documentation of R
functions for R packages. Contains both a comprehensive template
describing many of the available roxygen formatting, navigation, and
functionality tags, as well as a minimal template with bare-minimum
recommended tags and formatting for a single vs. family of functions.

## Installation

You can install the released version of rdocs from
[Bitbucket](https://bitbucket.com/robert_amezquita/rdocs) with:

``` r
devtools::install_bitbucket("robert_amezquita/rdocs")
```

## Example

``` r
## Complete template with exhaustive documentation of available tags
rdocs_r_summary() # creates R/rdocs_r_summary.R file
rdocs_r_minimal() # creates R/rdocs_r_minimal.R file
```

## Source code

Source code is available at
[Bitbucket](https://bitbucket.com/robert_amezquita/rdocs), where you can
file issues with suggestions, comments, or feedback.

## Contributing

Feel free to submit a pull request to contribute new templates or
enhance the existing templates with better documentation or suggestions
for new templates. Some ideas include data functions, common R motifs
(usage of tidyeval for example).
